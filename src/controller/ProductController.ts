import {NextFunction, Request, Response} from "express";
//import {getRepository, getConnectionManager} from "typeorm";
import { User } from "../entity/User";
import { ProductService } from '../services/product.service';
//const userRepository = getRepository(User);


export async function all(req: Request, res: Response) {    
    var rs = await ProductService.Ins.getAll()
    res.json(rs);
}